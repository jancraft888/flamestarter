
# jDev Studios Flamestarter

**Flamestarter** is a template project for learning 2D Game Development in JavaScript.

## Upgrading
To upgrade the flamestarter version, either update `flamestarter.js` for computer users, or update `index.html` (from `ipad_index.html`) for iPad users.

## Get Started
 * Install [git](https://git-scm.com/downloads)
 * Install [Python 3](https://www.python.org/downloads/)
> ⚠️ If you use the installer, on some operating systems it may require you to check a box to `Add it to your PATH`, it is very important that *python* is in your PATH.
 * *(optional)* Install [VS Code](https://code.visualstudio.com)
 * Open a terminal:


On *Windows*, you can open `cmd`, `powershell` or the better `Windows Terminal` *(find it in the Microsoft Store)*


On *Linux*, you should know how to do it, but most desktops name it `Terminal`


On *Mac*, you can open the `Terminal`, or the better `iTerm2`


On an *iPad*, install CodeSandbox and follow the [iPad Guide](./IPAD.md).


 * Verify that git is installed with `git --version`
 * Verify that Python is installed with `python -V` or `python3 -V` (remember which is correct to use it on next commands)
 * Change directory to your Desktop (or Documents) folder `cd /d %userpofile%\Desktop` (Windows only) or `cd ~/Desktop` (others)
 * Clone this git repo with `git clone https://gitlab.com/jancraft888/flamestarter.git`
 * CD into the directory with `cd flamestarter` then `cd src`
 * Open a code editor like VS Code here by using `code .`
 * Then run `python -m http.server`, this should start a local web server at [localhost:8000](http://localhost:8000/)!
 * You're ready! Try editing the `app.js` file and see what happens!
