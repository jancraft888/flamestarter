let myEntity

function setup() {
  let redMushroomTexture = loadImage('assets/mushroom-red.png')
  myEntity = {
    texture: redMushroomTexture,
    x: 0,
    y: 0,
    scale: 4
  }
}

function animate(delta) {
  if (key('w')) myEntity.y -= 80 * delta
  if (key('a')) myEntity.x -= 80 * delta
  if (key('s')) myEntity.y += 80 * delta
  if (key('d')) myEntity.x += 80 * delta

  drawEntity(myEntity)
}
