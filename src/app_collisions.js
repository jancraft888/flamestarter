let red, blue

function setup() {
  let redMushroomTexture = loadImage('assets/mushroom-red.png')
  let blueMushroomTexture = loadImage('assets/mushroom-blue.png')

  red = {
    texture: redMushroomTexture,
    x: -64,
    y: 0,
    scale: 4
  }

  blue = {
    texture: blueMushroomTexture,
    x: 64,
    y: 0,
    scale: 4
  }
}

function animate(delta) {
  if (key('w')) red.y -= 80 * delta
  if (key('a')) red.x -= 80 * delta
  if (key('s')) red.y += 80 * delta
  if (key('d')) red.x += 80 * delta

  if (key('i')) blue.y -= 80 * delta
  if (key('j')) blue.x -= 80 * delta
  if (key('k')) blue.y += 80 * delta
  if (key('l')) blue.x += 80 * delta

  preventCollision(red, blue)
  preventOutOfBounds(red)
  preventOutOfBounds(blue)

  drawEntities([ red, blue ])
}

