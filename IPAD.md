# iPad Flamestarter Guide

You have to install [CodeSandbox](https://apps.apple.com/us/app/codesandbox/id1423330822) and make an account.

1. Press on *Local Sandboxes* and then *New Sandbox*. Create a simple Node.js Application.
2. Press and hold the *index.js* file in the sidebar, then rename it to *app.js*.
3. Open the packages tab, at the bottom, resolve the `http-server` module.
4. Modify your *package.json* to look like the following:
```json
{
  "name": "my-flamestarter-project",
  "version": "1.0.0",
  "dependencies": {
    "http-server": "^14.1.1"
  },
  "scripts": {
    "dev": "http-server ."
  }
}
```
5. Create an *index.html* and put the content of `ipad_index.html` in it.
6. Copy `src/app.js` to *app.js*
7. Create the *assets* directory and put `src/assets/` inside of it.
8. Press the `dev: http-server .` button and then press the globe to open your browser.
9. Start editing the *app.js* file
